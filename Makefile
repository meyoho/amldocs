PWD=$(shell pwd)
TAG=dev-$(shell git config --get user.email | sed -e "s/@/-/")
IMAGE=index.alauda.cn/alaudak8s/amldocs
image=$(IMAGE)
tag=$(TAG)

.PHONE: setup
setup:
	chmod +x ./install.sh
	./install.sh

run: run-server run-sass

run-server:
	hugo server --renderToDisk --disableFastRender --cleanDestinationDir

run-sass:
	sass --watch themes/hugo-theme-docdock/static/scss/flex/style.scss:themes/hugo-theme-docdock/static/theme-flex/style.css

build-sass:
	sass themes/hugo-theme-docdock/static/scss/flex/style.scss:themes/hugo-theme-docdock/static/theme-flex/style.css

build-site:
	hugo -d public --cleanDestinationDir --config artifacts/config.toml

build-pdf:
	pkill hugo || true
	ulimit -n 2048
	$(MAKE) run-server& sleep 5
	$(MAKE) build-pdf-file
	pkill hugo

build-pdf-file:
	ulimit -n 2048
	wkhtmltopdf --dpi 300 -s A4 --user-style-sheet static/pdf.css --disable-external-links --disable-forms --disable-internal-links --javascript-delay 20000 --footer-right "[page] / [toPage]"  --no-stop-slow-scripts  cover static/cover.html  $(shell linkcrawler links) static/docs.pdf

build-docker:
	# cp artifacts/Caddyfile public
	docker build -t ${IMAGE}:${TAG} -f artifacts/Dockerfile .
	# docker push ${IMAGE}:${TAG}

build: build-sass build-site build-docker

update:
	kubectl set image -n alauda-system deploy/aml-docs backend=${IMAGE}:${TAG}
	kubectl scale -n alauda-system deploy/aml-docs --replicas=0
	kubectl scale -n alauda-system deploy/aml-docs --replicas=1

build-deploy: build
	$(MAKE) image=${IMAGE} tag=${TAG} update

k8sinit:
	kubectl apply -f artifacts/deploy.yaml
	kubectl apply -f artifacts/ingress.yaml
