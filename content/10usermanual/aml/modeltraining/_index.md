+++
title = "模型训练"
description = "根据 TensorFlow 或 PyTorch 框架，创建模型训练的任务。使用 Jenkins 实例，构建并执行任务，并输出日志和模型文件。"
weight = 4
+++

算法工程师完成算法开发后进行模型训练，根据 TensorFlow 或 PyTorch 框架，创建模型训练的任务。使用 Jenkins 实例，构建并执行任务，并输出日志和模型文件。目前只支持使用 TensorFlow 框架进行模型训练。

平台为每个分布式 TensorFlow 模型训练任务提供了 TensorBoard 服务，TensorBoard 通过读取 TensorFlow 的事件文件来运行。TensorFlow 的事件文件包括了在 TensorFlow 运行中涉及到的主要数据。

{{%children style="card" description="true" %}}