+++
title = "模型开发"
description = "创建开发环境，完成算法编写、执行、调试等工作。支持对开发环境的更新和删除。"
weight = 3
+++

创建模型开发环境，完成算法编写、执行、调试等工作。支持对开发环境的更新和删除。

模型开发环境的实例运行依赖容器平台。 

**准备工作**：此功能需要 Jupyter Notebook 镜像，因此管理员可以在 DevOps 平台的管理视图中，为项目绑定镜像仓库及相关凭据，保存 Jupyter Notebook 镜像。

{{%children style="card" description="true" %}}