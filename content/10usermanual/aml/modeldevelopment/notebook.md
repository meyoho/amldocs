+++
title = "使用 Jupyter Notebook"
description = "通过集成 Jupyter Notebook 的开发环境，您可以调用第三方包来进行代码开发、调测和运行，从而完成机器学习服务解决方案。"
weight = 3
+++

通过集成 Jupyter Notebook 的开发环境，您可以调用第三方包来进行代码开发、调测和运行，从而完成机器学习服务解决方案。

**操作步骤**

1. 通过单击已创建模型开发环境中的 Jupyter Notebook 的地址，跳转到 Jupyter Notebook。

4. 在 Jupyter 的 **Files** 下，找到 PVC 中 `claimName` 的文件夹。  
其中 ***用户的登录名*** 和 **project** 这两个文件夹的内容与 AML 中 **数据管理** 的文件内容是同步的，即在 **数据管理** 中编辑更新的内容会同步在 Jupyter 中显示，在 Jupyter 中编辑更新的内容会同步在 AML 中的 **数据管理** 中显示。  
当其他用户登录 AML 时，这两个文件夹对其他用户是不可见的状态。  
除了这两个文件夹，Jupyter 中其他文件夹在 AML 中的 **数据管理** 中也不显示。

5. 单击文件夹下的 `.ipynb` 文件，进入编辑模式。

	![jupiteredit](/img/jupiteredit.png?classes=big)

	参考[Jupyter Notebook 文档](https://jupyter-notebook.readthedocs.io/en/stable/)。