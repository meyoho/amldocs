+++
title = "模型发布"
description = "在机器学习中，在完成模型训练输出模型文件后，可以执行模型发布，发布模型服务。"
weight = 5
+++

在机器学习中，在完成模型训练输出模型文件后，可以创建模型发布任务。

执行模型发布任务，可发布模型服务。



{{%children style="card" description="true" %}}