+++
title = "创建模型发布任务并执行发布"
description = "当模型训练输出模型文件后，可以创建并执行发布任务，以便发布模型服务。"
weight = 1
+++

当模型训练输出模型文件后，可以创建并执行发布任务，以便发布模型服务。

**操作步骤**

1. 登录平台，进入业务视图后，在项目列表页面，单击一个 ***项目名称***，再单击 **模型发布**。

3. 单击 **创建发布任务**。在 **请选择框架类型** 窗口，选择 **TensorFlow** 或 **PyTorch**。   
	
4. 在创建发布任务页面，在 **基本信息** 区域：
		
	* **名称**：名称支持小写英文字母、数字 0 ~ 9、中横线。字符数大于等于 1 个，小于等于 32 个。不支持以中横线开头或结尾。
		
	* **显示名称** (非必填)：支持中文字符，字符数小于等于 32 个。如未输入，默认显示为空。
		
4. 在 **镜像信息** 区域，支持构建新镜像和使用已有镜像的方式配置镜像信息，PyTorch 框架仅支持使用已有镜像。
		
	![publishcreate](/img/publishcreate.png?classes=big)
				
	* **构建镜像**：构建镜像是根据参数和基础镜像构建的模型发布镜像。在 **模型文件位置** 区域，选择 **数据管理** 或 **代码仓库**。
		
		* **数据管理**：适用于模型文件存放在 AML 平台的 **数据管理** 中。
		
			* **构建环境**：选择执行构建镜像的命名空间。命名空间所在集群必须已指定 Jenkins，命名空间已绑定 PVC。
			
			* **模型文件路径**：输入模型文件在 **数据管理** 中的位置；或单击 **选择路径**，选择模型文件在 **数据管理** 中的位置，单击 **确定**。
			
			* **基础镜像**：支持选择或输入要用于模型发布的基础镜像地址，并提供多个版本的镜像。单击 **选择**，在 **选择镜像** 窗口：
				
				* 选择：在 **镜像仓库** 框中，选择一个要使用的镜像仓库中的镜像，并在后面的框中选择或输入镜像的 Tag。
				
				* 输入：手动输入要使用的镜像仓库，例如当要选择的镜像没有保存在已绑定的制品仓库中。在 **镜像地址** 框中，输入要使用的镜像仓库地址。如果访问的镜像需要验证，在 **凭据** 框中，选择已创建可访问要使用镜像仓库的凭据。凭据类型支持 dockerconfigjson。

					注意：使用第三方镜像时，确认是否可以直接拉取。可以通过 docker pull 命令拉取。镜像仓库地址格式：index.alauda.cn/library/hello-world:latest。
			
			* **发布镜像**：选择使用的镜像，用于执行流水线构建。发布镜像是使用模型训练产生的模型文件和模型发布的基础镜像构建出来的镜像，用于执行流水线构建，构建成功后，模型文件会被输出到 **数据管理** 中。支持选择或输入发布镜像地址，并提供多个版本的镜像。单击 **选择**，在 **选择镜像** 窗口：
				
				* 选择：在 **镜像仓库** 框中，选择一个镜像，并在后面的框中选择或输入镜像的 Tag。
				
				* 输入：手动输入镜像仓库地址，例如当要选择的镜像没有保存在已绑定的制品仓库中。在 **镜像地址** 框中，输入要使用的镜像仓库地址。如果访问的镜像需要验证，在 **凭据** 框中，选择已创建可访问要使用镜像仓库的凭据。凭据类型支持 dockerconfigjson。

					注意：使用第三方镜像时，确认是否可以直接拉取。可以通过 docker pull 命令拉取。镜像仓库地址格式：index.alauda.cn/library/hello-world:latest。
					
			* **构建上下文**：输入构建过程可以引用上下文中的任何文件路径。例如，构建中可以使用 COPY 命令在上下文中引用文件路径 ./filepath 中的任何文件。
		
			* **构建参数** (非必填)：输入更多自定义的构建参数，对镜像构建进行更详细的配置。例如 `--build-arg MODEL=test`，其中 `MODEL` 的值 test 就是模型文件的文件名。
			
			* **重试次数** (非必填)：输入如果生成镜像失败时的重试次数。默认值是 3。
				
		* **代码仓库**：适用于模型文件存放在代码仓库中。
		
			* **代码仓库**：单击 **选择** 后，在 **代码仓库** 窗口，选择是否使用已绑定的代码仓库或输入一个未绑定的代码仓库，选择并填写以下参数后，单击 **确定**。
			
				* 使用已绑定的代码仓库：单击 **选择**，在 **代码仓库** 框中，选择一个项目已绑定的代码仓库。
				
				* 使用未绑定的代码仓库：单击 **输入**，选择仓库类型后，在 **代码仓库地址** 框中，输入 Git 代码仓库地址，例如 `https://github.com/example/example.git`，或 SVN 代码仓库地址 `http://svn-company.mycompany.cn/example`。  
在 **凭据** 框中，选择一个已创建的检出代码仓库时需要使用的凭据，用于匹配发送请求的用户身份。如果凭据列表中没有要使用的凭据，需要先创建凭据。
		
			* **代码分支**：代码所在的分支，例如 master。
			
			* **相对目录**：输入模型文件在代码仓库中的目录。
			
			* **基础镜像**：支持选择或输入要用于模型训练的基础镜像地址，并提供多个版本的镜像。单击 **选择**，在 **选择镜像** 窗口，配置以下信息后，单击 **确定**。
				
				* 选择：在 **镜像仓库** 框中，选择一个要使用的镜像仓库中的镜像，并在后面的框中选择或输入镜像的 Tag。
				
				* 输入：手动输入要使用的镜像仓库，例如当要选择的镜像没有保存在已绑定的制品仓库中。在 **镜像地址** 框中，输入要使用的镜像仓库地址。如果访问的镜像需要验证，在 **凭据** 框中，选择已创建可访问要使用镜像仓库的凭据。凭据类型支持 dockerconfigjson。

					注意：使用第三方镜像时，确认是否可以直接拉取。可以通过 docker pull 命令拉取。镜像仓库地址格式：index.alauda.cn/library/hello-world:latest。
			
			* **发布镜像**：选择使用的镜像，用于执行流水线构建。发布镜像是使用模型训练产生的模型文件和模型发布的基础镜像构建出来的镜像，用于执行流水线构建，构建成功后，模型文件会被输出到 **数据管理** 中。支持选择或输入发布镜像地址，并提供多个版本的镜像。单击 **选择**，在 **选择镜像** 窗口，配置以下信息后，单击 **确定**。
				
				* 选择：在 **镜像仓库** 框中，选择一个镜像，并在后面的框中选择或输入镜像的 Tag。
				
				* 输入：手动输入镜像仓库地址，例如当要选择的镜像没有保存在已绑定的制品仓库中。在 **镜像地址** 框中，输入要使用的镜像仓库地址。如果访问的镜像需要验证，在 **凭据** 框中，选择已创建可访问要使用镜像仓库的凭据。凭据类型支持 dockerconfigjson。

					注意：使用第三方镜像时，确认是否可以直接拉取。可以通过 docker pull 命令拉取。镜像仓库地址格式：index.alauda.cn/library/hello-world:latest。
	
			* **构建上下文**：输入构建过程可以引用上下文中的任何文件路径。例如，构建中可以使用 COPY 命令在上下文中引用文件路径 ./filepath 中的任何文件。
		
			* **构建参数** (非必填)：输入更多自定义的构建参数，对镜像构建进行更详细的配置。例如 `--build-arg MODEL=test`，其中 `MODEL` 的值 test 就是模型文件的文件名。
			
			* **重试次数** (非必填)：输入如果生成镜像失败时的重试次数。默认值是 3。
			

	* **已有镜像**：使用已有镜像配置镜像信息。在 **发布镜像** 框中，单击 **选择**，在 **选择镜像** 窗口，配置以下信息后，单击 **确定**。发布镜像是使用模型训练产生的模型文件和模型发布的基础镜像构建出来的镜像，用于执行流水线构建，构建成功后，模型文件会被输出到 **数据管理** 中。
				
		* 选择：在 **镜像仓库** 框中，选择一个镜像，并在后面的框中选择或输入镜像的 Tag。
				
		* 输入：手动输入镜像仓库地址，例如当要选择的镜像没有保存在已绑定的制品仓库中。在 **镜像地址** 框中，输入要使用的镜像仓库地址。如果访问的镜像需要验证，在 **凭据** 框中，选择已创建可访问要使用镜像仓库的凭据。凭据类型支持 dockerconfigjson。

			注意：使用第三方镜像时，确认是否可以直接拉取。可以通过 docker pull 命令拉取。镜像仓库地址格式：index.alauda.cn/library/hello-world:latest。

4. 在 **发布信息** 区域，在 **发布环境** 框中，选择执行模型发布任务的命名空间。命名空间所在集群必须已指定 Jenkins，命名空间已绑定 PVC。

5. 单击 **创建**。

8. 创建成功后，在模型发布详情页面的 **发布记录** 区域，单击 **发布**，设置执行参数。配置完成后，单击 **执行**。

	* **发布环境**：用于执行发布任务的命名空间。
	
	* **部署名称**：运行模型服务的 Deploymet 名称。

	* **模型名称**：模型服务的名称。
	
	* **模型版本**：自定义的模型服务版本号。支持正整数。
		
	* **发布镜像**：支持修改发布镜像的 tag。

	* **实例数**：部署的 Pod 数量。
	
	* **容器限制**：输入容器对 CPU 资源和内存资源的限制值。CPU 单位：核。内存单位：MB。请求值指容器使用的最小资源需求，作为容器调度时资源分配的判断依据，即只有当节点上可分配资源量大于或等于容器资源请求数时，才会允许将容器调度到该节点。限制值指容器能使用资源的最大值，如果设置为 0，表示不对资源做限制。    
限制值（limit）表示限制 Pod 在运行时，最多可使用的内存和 CPU 计算资源。请求值小于等于限制值。

	以下参数仅在镜像模式为 **构建镜像** 时适用。
	
	* **模型文件路径**：输入模型文件在 **数据管理** 中的位置；或单击 **选择路径**，选择模型文件在 **数据管理** 中的位置，单击 **确定**。

	* **构建环境**：选择执行构建镜像的命名空间。命名空间所在集群必须已指定 Jenkins，命名空间已绑定 PVC。
	
	以下参数仅在镜像模式为 **已有镜像** 时适用。

	* **gRPC 端口**：输入对外提供 gRPC 服务的端口。

	* **restful 端口**：输入对外提供 restful 服务的端口。

	* **执行命令**：如果镜像中未设置 ENTRYPOINT，可根据需要输入启动命令。

	* **参数**：执行命令所需的匹配参数。

	* **组件标签**：为要发布的模型服务版本 Pod 实例，添加标签。

	* **环境变量**：设置容器环境变量。

	* **集群内访问**：添加集群内访问的端口映射规则。
	
		* **名称**：设置映射规则的名称。
		
		* **协议**：端口协议，支持选择 **TCP** 和 **UDP**。
		
		* **服务端口**：集群内部暴露的服务端口号。

		* **容器端口**：服务端口映射的目标端口号，即组件中的 Pod 对外暴露业务访问的端口号。

9. 执行后，查看流水线状态，包括 **创建中**、**运行中**、**已完成**、**已终止**、**删除中**、**已失败**。
