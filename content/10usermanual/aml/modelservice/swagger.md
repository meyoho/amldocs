+++
title = "使用 Swagger 测试 API"
description = "使用 Swagger，测试 API 构建。"
weight = 4
+++

使用 Swagger，测试 API 构建。

**操作步骤**

1. 登录平台，进入业务视图后，在项目列表页面，单击一个 ***项目名称***，再单击 **模型服务**。

4. 在模型服务页面，单击某个模型服务，进入到模型服务的详情页面。

6. 在 **模型服务** 区域，单击要查看详情的服务名称，跳转到该条执行记录的详情页。

7. 在某个版本的 **部署信息** 区域，当应用状态是可用时，点击 **API 测试** > **Swagger**，跳转到 Swagger，测试 API 构建。       
		Swagger 用于生成、描述、调用和可视化 RESTful 风格的 Web 服务。Swagger的目标是对 REST API 定义一个标准的和语言无关的接口，让人和计算机无需访问源码、文档或网络流量监测就可以发现和理解服务的能力。

8. 在 Swagger 新窗口，查看模型服务调用方法，选取一个进行 API 测试。单击 **Try it out**。

	![swagger](/img/swagger.png?classes=big)

9. 再单击 **Execute**。查看执行结果。

	![swaggerresponse](/img/swaggerresponse.png?classes=big)