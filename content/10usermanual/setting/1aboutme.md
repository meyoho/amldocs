+++
title = "查看个人信息及平台 Token"
description = "查看当前账号的个人信息及平台 Token。"
weight = 1
+++

查看当前登录账号的个人信息以及平台 Token。


**操作步骤**

1. 登录平台后，单击页面右上方的 ***账号名称*** > **个人信息**，打开个人信息页面。

2. 单击 Token 属性右侧的  **平台 token**，在弹出的对话框中可查看、复制平台 Token，并了解 Token 的使用方法。

	**说明**：当用户通过 API 访问平台时，可使用平台提供的 Token 进行身份认证。



