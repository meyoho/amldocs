+++
title = "设置默认 Jenkins"
description = ""
weight = 3
+++

设置默认的 Jenkins 工具，在模型训练、模型发布中使用“已有镜像”时，将使用默认 Jenkins 工具执行流水线。

每个项目只能设置一个默认 Jenkins。

**前提条件**

1. 已为集群指定 Jenkins 工具。

**操作步骤**

1. 登录平台，进入管理视图后，单击左导航栏中的 **项目**。

2. 在项目列表页面，单击要设置默认 Jenkins 工具的 ***项目名称***。

3. 在项目详情页面的 **Jenkins** 区域，找到要设置默认的 Jenkins 工具，单击对应的 ![operations](/img/operations.png)，再单击 **设为默认**。

**后续操作**

1. 在当前默认 Jenkins 工具的名称后，具有 ![setdefault](/img/setdefault.png) 标记。

2. 如要更换默认 Jenkins，可设置其它集群指定的工具为默认 Jenkins 工具。