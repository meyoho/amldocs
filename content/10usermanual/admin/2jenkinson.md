+++
title = "集群指定 Jenkins"
description = ""
weight = 2
+++

为业务集群指定 Jenkins 工具，目前要求在同一项目下，每个集群只能指定一个 Jenkins 工具。

在模型训练、模型发布中使用“构建模式”时，需要指定使用构建环境所属集群的Jenkins 工具执行流水线。

**前提条件**

1. 执行指定 Jenkins 前，在 DevOps 平台上集成 Jenkins 工具，并绑定到相应的项目上。

**操作步骤**

1. 登录平台，进入管理视图后，单击左导航栏中的 **项目**。

2. 在项目列表页面，单击要指定 Jenkins 的 ***项目名称***。

3. 在项目详情页面的 **Jenkins** 区域，找到要绑定 Jenkins 工具的集群，单击对应的 ![operations](/img/operations.png)，再单击 **指定 Jenkins**。

4. 在 **指定 Jenkins** 窗口，在 **Jenkins** 框中，选择一个要绑定的 Jenkins 工具。

5. 单击 **指定**。

**后续操作**

1. 指定 Jenkins 后，管理员可根据实际情况，设置默认 Jenkins，参考 [设置默认 Jenkins]({{< relref "10usermanual/admin/3jenkinsdefault.md" >}})。

