+++
title = "查看项目详情"
description = ""
weight = 1
+++

在项目的详情页面，查看项目的基本信息，管理与项目绑定的工作和资源。

**操作步骤**

1. 登录平台，进入管理视图后，单击左导航栏中的 **项目**。

2. 在项目列表页面，单击要查看详情的 ***项目名称***。

3. 查看项目的基本信息。例如：项目名称、显示名称、状态、创建人、创建时间、更新时间、项目管理员、描述。

4. 在 **Jenkins** 区域，管理与项目绑定的集成工具。参考 [集群指定 Jenkins]({{< relref "10usermanual/admin/2jenkinson.md" >}}) 和 [设置默认 Jenkins]({{< relref "10usermanual/admin/3jenkinsdefault.md" >}})。

5. 在 **命名空间** 区域，管理与项目下命名空间绑定的持久卷声明（PVC）资源。参考 [命名空间绑定/解绑 PVC]({{< relref "10usermanual/admin/4pvcon.md" >}})。
