+++
title = "项目基础操作"
description = "项目的基础操作包括：创建项目、查看项目的详情、更新项目的基本信息、更新项目配额、添加/移除项目关联的集群、删除项目。"
weight = 1
alwaysopen = false
+++

平台管理员可对项目进行管理操作，例如：创建项目、查看项目的详情、更新项目的基本信息、更新项目配额、添加/移除项目关联的集群、删除项目。

{{%children style="card" description="true" %}}


