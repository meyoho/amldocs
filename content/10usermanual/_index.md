+++
title = "用户手册"
description = ""
weight = 1
+++

希望本文能够帮助您便捷地操作机器学习平台，管理各种资源。

## 功能成熟度说明

平台上的功能，按照成熟度可分为 Alpha 版本、Beta 版本、GA（General Availability）版本。

Beta 版本、GA（General Availability）版本的功能已趋于稳定或正在稳定迭代的过程当中，有专业的团队提供保障和服务，您可以根据实际的需要在生产环境中使用相应的功能。

Alpha 版本的功能，可能存在缺陷或严重 Bug，默认不在生产环境中开启，仅建议用于体验或测试平台功能时所搭建的环境。

为了方便您区分 Alpha 版本的功能，文档左侧的目录结构中，会以在相应功能模块或具体功能的导航菜单上标注 `Alpha` 的方式，提示您哪些是  Alpha 版本的功能。例如：创建子网（Alpha）。

平台功能成熟度的说明及建议使用的范围请参见下表。

**注意**：**机器学习平台的所有业务视图和管理视图功能，均是 Alpha 版本。**

| 版本      | 成熟度说明                                                   | 建议使用范围                                                 |
| :-------- | :----------------------------------------------------------- | :----------------------------------------------------------- |
| **Alpha** | 可能是有缺陷的，可能包含错误，启用后可能会遇到严重 Bug；<br>支持的功能可能在没有通知的情况下随时删除；<br>API、产品场景化封装、交互流程等更改可能会带来兼容性问题，但是，在后续的版本发布时不会特意声明。<br>依赖的 Kubernetes 功能如果是 Alpha 版本，相应的平台功能只能为 Alpha 版本。 | 私有环境部署时，默认关闭 Alpha 版本功能，如果需要，部署时可通过配置相关参数打开。<br>由于存在 Bugs 风险，同时，可能不会做长期的支持，推荐在短暂的功能体验或测试环境中使用，例如：POC 环境。 |
| **Beta**  | 可能存在缺陷，所有已支持的功能不会被轻易删除。<br>API 参数、产品设计细节等可能会随版本迭代发生变化。出现这种情况时，我们将提供迁移到下一个版本的说明。执行编辑操作时需要谨慎操作，可能需要停用依赖该功能的应用程序。 | 默认开启 Beta 版本功能，部署时可通过配置相关参数关闭。<br>后续版本中可能存在不兼容的更改，建议仅用于非关键型业务运行环境。如果有多个可以独立升级的集群，则可以放宽此限制，在生产环境使用。 |
| **GA**    | 功能的稳定版本，将出现在许多后续版本中。<br>API 参数、产品设计细节等可能会随长期支持版本发生变化。出现这种情况时，我们将提供迁移到下一个版本的说明。 | 适用于所有环境，包括生产环境。                               |
